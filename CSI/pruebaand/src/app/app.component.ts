import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var utils: any;
declare var require: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pruebaandss';
  modeloTextos: {};

  ngOnInit() {
    utils.init();
    utils.countCharacter("textarea-example", 255);
    this.obtenerTextos();

  }

  /**
   * Método encargado de obtener la información temática que se cargará en la página.
   * @author Juan David Cardona Carmona
   * @date 2020-09-15.
   * @param    
   */
  obtenerTextos() {
    try {
      var obj = this;
      localStorage.setItem('textos', JSON.stringify(require("./textos.json")));
      this.modeloTextos = JSON.parse(localStorage.getItem('textos'));      
    } catch (e) {
      console.log("Error en AppComponent-webConfigLocal-> " + e);
    }
  }

  /**
   * Método encargado de eliminar la notificación que aparece en la parte superior de color naranja.
   * @author Juan David Cardona Carmona
   * @date 2020-09-15.
   * @param    
   */
  cerrarMensaje() {
    var elem = document.getElementById('mensaje');
    elem.parentNode.removeChild(elem);
  }
}
